#!/bin/sh

source /cvmfs/sft.cern.ch/lcg/views/LCG_99/x86_64-centos7-gcc10-opt/setup.sh

#BACSEL=/afs/cern.ch/user/t/thubrech/private/bacsel/bacsel
BACSEL=/data/thubrech/bacsel/bacsel.exe

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/data/thubrech/bacsel/lib/

F_RANGE=(1 2 3 4 5)
N_RANGE=(29 31 33 35)

for F in ${F_RANGE[@]}; do
  for n in ${N_RANGE[@]}; do
    # y = n * 2^{-F} in hex
    Y=$(echo "obase=16;(2^(-$F)*$n)" | bc -l | tr A-F a-f | xargs -I _ echo 0x_p0 | ./convert)

    for E_IN in $(seq 0 $(echo "$F - 1" | bc -l)); do
      # Create the directory in which the results will be stored
      mkdir -p $Y

      OUT=$Y/$E_IN
      touch $OUT

      E_MIN=$(echo "min_exp($F, $n)" | bc -l util.bc)

      for k in $(seq 1 $E_MIN); do
        echo "k: $k, n: $n, F: $F"
        T0=$(echo "limit($k, $F, $n)" | bc -l util.bc)
        T1=$(echo "limit($k - 1, $F, $n)" | bc -l util.bc)

        $BACSEL -rnd_mode all -prec 128 -n 53 -nn 53 -m 50 -t 20.9 -t0 $T0 -t1 $T1 -y $Y -d 2 -alpha 2 -v -e_in $E_IN -nthreads 48 | grep -v 'distance is: 0' >> $OUT 2>&1
      done
    done
  done
done

