#!/bin/sh

for i in $(ls); do
  if [ $i != "export.sh" ]; then
    cat $i/* | awk '{print $2,$5}' | sed 's/x=//' | sed "s/,/, $i #/";
  fi
done
